# SerialBox Docker Compose

## First things first...
Make sure to change the .env file if you intend on using these
containers in production.  I'll remind you again later.  :-)

## Get SerialBox up and running in Docker in minutes.

This docker compose file will allow you to quickly create a running set of 
two docker containers with one container running SerialBox as a Django app using
*uwsgi* on *nginx* with Python 3.5 and the other running the latest
docker hub version of the [PostgreSQL](https://hub.docker.com/_/postgres/)
database as a back-end.  

## Clone this repo

    git clone https://gitlab.com/serial-lab/serial-box-docker-compose.git


## Edit the .env file
The .env file populates key environment variables in the docker containers
that allow the web application to communicate with the database 
container.  **Make sure you do not forget to change these if you are using
the docker-compose file in a production environment...unless you want
everyone to know your password.**

The .env file has the following environment variables:

Environment Variable    |   Description
--------------------    |   --------
SERIALBOX_USER          |   The SerialBox database user.
SERIALBOX_PASSWORD      |   The SerialBox database user's password.
SERIALBOX_DB            |   The name of the SerialBox database.
SERIALBOX_DB_HOST       |   The host of the database (not required- default is localhost)
SERIALBOX_DB_PORT       |   The port of the database (not required- default is 5432)

## Command Line Usage

    docker-compose build
    docker-compose up -d
    docker-compose run web python manage.py collectstatic
    docker-compose run web python manage.py migrate
    docker-compose run web python manage.py createsuperuser
    
This will bring up a running SerialBox/PostgreSQL application on listening
on port 80.  Navigate to:

    http://localhost
    
Make sure you do not have any other applications using port 80 or this will
obviously fail.  To change the port, modify the first 80 in the 80:80 within
the compose file.